const coursesData = [

	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Lorem ipsum dolor sit, amet, consectetur adipisicing elit. Quidem laudantium obcaecati deserunt omnis, minima harum maiores quaerat alias repellendus ab earum illo veritatis at deleniti fugit atque, voluptas molestias voluptates!",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python-Django",
		description: "Lorem ipsum dolor sit, amet, consectetur adipisicing elit. Quidem laudantium obcaecati deserunt omnis, minima harum maiores quaerat alias repellendus ab earum illo veritatis at deleniti fugit atque, voluptas molestias voluptates!",
		price: 55000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Lorem ipsum dolor sit, amet, consectetur adipisicing elit. Quidem laudantium obcaecati deserunt omnis, minima harum maiores quaerat alias repellendus ab earum illo veritatis at deleniti fugit atque, voluptas molestias voluptates!",
		price: 60000,
		onOffer: true
	}
]

export default coursesData;

