import {useState} from 'react'

import {Card, Button} from 'react-bootstrap'

export default function CourseCard({courseProp}) {

console.log(courseProp)

// object destructuring 
const {name, description, price} = courseProp
// Syntax: const {properties} = propname

// array destructuring
const [count, setCount] = useState(0)


// // // Syntax: const [getter, setter] =useState(initialValue)

// // // Hook used is useState- to store the state


// function enroll(){

//     setCount(count);
//     console.log('Enrollees' + count)

// }


function enroll() {
   let numberOfSeat = setCount(count + 1)
    if (count !== 30) {
        // alert('Adding enrollees');
    } else {
        alert('No more seat available');
    }
}




    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Card.Text>Enrollees: {count}</Card.Text>
                <Button variant="primary" onClick = {enroll}>Enroll</Button>
            </Card.Body>
        </Card>
    )
}